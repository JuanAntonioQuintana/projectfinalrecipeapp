import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { storage } from 'firebase';
import { UsersService } from '../../services/users.service';
import { AboutPage } from '../about/about';
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user = {id: null, email: null, password: null, name: null, username: null, image:null, recipesUpload:[], favourites:[], numberOfRecipe:null, date: null}
  photo: string = '';
  referencePhoto: string

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera:Camera, public usersService: UsersService) {
  }

  async registerUser(){
    this.user.id = Date.now();
    var storageRef = storage().ref();
    var file = this.photo;
    this.referencePhoto = 'images/pictures'+Date.now()+'.jpg';
    var mountainImagesRef = storageRef.child(this.referencePhoto);
    const uploadImage = (file) => {
      return new Promise((resolve, reject) => {
        mountainImagesRef.putString(file, 'data_url').then((snapshot)=> {
           resolve();
        });
      });
    }

    await uploadImage(file);

    const downloadImage = () => {
      return new Promise((resolve, reject) => {
        storageRef.child(this.referencePhoto).getDownloadURL().then((url)=>{
          this.user.image = url;
          resolve();
        });
      });
    }

    await downloadImage();


    this.usersService.createUser(this.user);
    this.navCtrl.push(AboutPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  configureImage(image) {
    
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }
    //this.recipe.date = new Date(Date.now()).toLocaleString();
    //this.recipe.date = new Date().toLocaleDateString();
    this.user.date = new Date().toISOString().slice(0, 10)
    this.camera.getPicture(options).then((imageData) => {
      this.photo = 'data:image/jpeg;base64,' + imageData;

    }, (err) => {
      
    })
  }

}
