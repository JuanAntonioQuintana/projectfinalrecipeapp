import { ProfilePage } from './../profile/profile';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { UsersService } from '../../services/users.service';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  public email: string;
  public password: string;
  public error: string = '';
  public result: Array<any> = [];
  users = [];

  @ViewChild("myNav") nav: NavController

  constructor(public navCtrl: NavController, public navParams: NavParams, public usersService: UsersService) {
    usersService.getUsers().valueChanges().subscribe(data => this.users = data);
  }

  submitLogin(){
    this.result = this.users.filter(user => user.email === this.email && user.password === this.password);
    if(this.result.length > 0){
      this.navCtrl.push(ProfilePage, {user: this.result});
    }
  }

  redirectToSignup(){
    this.navCtrl.push(RegisterPage);
  }
}
