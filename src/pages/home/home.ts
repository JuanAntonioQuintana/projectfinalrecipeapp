import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RecipesService } from '../../services/recipes.service';
import { RecipeDetailPage } from '../recipe-detail/recipe-detail';
import { informationAboutUser } from "../profile/profile";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  recipes = [];
  user: Array<any> = [];

  @ViewChild("myNav") nav: NavController

  constructor(public navCtrl: NavController, public recipesService: RecipesService) {
    recipesService.getRecipes().valueChanges().subscribe(data => this.recipes = data);
    this.user = informationAboutUser;
  }

  onSelect(recipe): void {
    this.navCtrl.push(RecipeDetailPage, {recipe: recipe});
  }

}
