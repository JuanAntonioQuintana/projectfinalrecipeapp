import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { informationAboutUser } from "../profile/profile";
import { RecipesService } from '../../services/recipes.service';
import { UsersService } from '../../services/users.service';
import { RecipeDetailPage } from '../recipe-detail/recipe-detail';

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {
  user: Array<any> = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public recipesService: RecipesService, public usersService: UsersService) {
    this.user = informationAboutUser;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }

  onSelect(recipe){
    this.navCtrl.push(RecipeDetailPage, {recipe: recipe});
  }
  remove(recipe){
    this.user[0].favourites = this.user[0].favourites.filter(item => item.id !== recipe.id);
    this.usersService.editUser(this.user[0]);

  }

}
