import { RecipesService } from './../../services/recipes.service';
import { UsersService } from './../../services/users.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { storage } from 'firebase';
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  user: Array<any> = [];
  userBeforeChange: Array<any> = [];
  recipes = [];
  recipesCopy = [];
  photo: string = '';
  referencePhoto: string

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera:Camera, public usersService: UsersService, public recipesService: RecipesService, private toastCtrl: ToastController) {
    this.user = this.navParams.get("user");
    this.photo = this.user[0].image;
    this.recipesService.getRecipes().valueChanges().subscribe(data => this.recipes = data);
    this.userBeforeChange = this.user[0].username;
  }

  async modifyUser(){
    var storageRef = storage().ref();
    if(this.photo === this.user[0].image){
      
      const updateRecipes = () => {
        return new Promise((resolve, reject) => {
          this.recipesCopy = this.recipes.filter(recipe => recipe.creator === this.userBeforeChange);
          var i = 0
          for(i = 0;i < this.recipesCopy.length;i++){
            this.recipesCopy[i].creator = this.user[0].username;
            this.recipesService.editRecipe(this.recipesCopy[i]);
          }
          if(i > this.recipesCopy.length){
            
            resolve();
          }
        })
      }
      updateRecipes();
      this.usersService.editUser(this.user[0]);
      this.recipesService.getRecipes().valueChanges().subscribe(data => this.recipes = data);
      this.userBeforeChange = this.user[0].username;   
    } else { 
      var file = this.photo;
      this.referencePhoto = 'images/pictures'+Date.now()+'.jpg';
      var mountainImagesRef = storageRef.child(this.referencePhoto);
      const uploadImage = (file) => {
        return new Promise((resolve, reject) => {
          mountainImagesRef.putString(file, 'data_url').then((snapshot)=> {
            resolve();
          });
        });
      }
      await uploadImage(file);
      const downloadImage = () => {
        return new Promise((resolve, reject) => {
          storageRef.child(this.referencePhoto).getDownloadURL().then((url)=>{
            this.user[0].image = url;
            resolve();
          });
        });
      }
      await downloadImage();
      const updateRecipes = () => {
        return new Promise((resolve, reject) => {
          this.recipesCopy = this.recipes.filter(recipe => recipe.creator === this.userBeforeChange);
          var i = 0
          for(i = 0;i < this.recipesCopy.length;i++){
            this.recipesCopy[i].creator = this.user[0].username;
            this.recipesCopy[i].imageCreator = this.user[0].image;
            this.recipesService.editRecipe(this.recipesCopy[i]);

          }
          if(i > this.recipesCopy.length){
            resolve();
          }
        })
      }
      updateRecipes();
      this.usersService.editUser(this.user[0]);
      this.recipesService.getRecipes().valueChanges().subscribe(data => this.recipes = data);
      this.userBeforeChange = this.user[0].username;
    }
    this.presentToast();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Perfil actualizado correctamente',
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  configureImage(image) {
    
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }
    
    this.camera.getPicture(options).then((imageData) => {
      this.photo = 'data:image/jpeg;base64,' + imageData;

    }, (err) => {
      
    })
  }

}
