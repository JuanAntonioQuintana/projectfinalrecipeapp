import { FavoritesPage } from './../favorites/favorites';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';
import { RecipesPage } from '../recipes/recipes';
import { NewRecipePage } from './../new-recipe/new-recipe';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})

export class TabsPage {
  tab1Root = HomePage;
  tab2Root = RecipesPage;
  tab3Root = NewRecipePage;
  tab4Root = FavoritesPage;
  tab5Root = AboutPage;

  constructor() {}

}

