import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { RecipesService } from '../../services/recipes.service';
import { UsersService } from '../../services/users.service';
import { storage } from 'firebase';
import { informationAboutUser } from "../profile/profile";
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-new-recipe',
  templateUrl: 'new-recipe.html',
})
export class NewRecipePage {

  user: Array<any> = [];
  recipe = {id: null, title: null, description: null, images: [], ingredients: null, productions: null, date: null, creator: null, likes: 0, imageCreator: null }
  photos: Array<any> = [];
  photosToUpload: Array<any> = [];
  referencePhoto: string;
  referenceUrl: string;
  recipesImages: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera:Camera, public recipesService: RecipesService, public usersService: UsersService, private toastCtrl: ToastController) {
    this.user = informationAboutUser;
    if(this.user.length > 0){
      if(this.user[0].recipesUpload === undefined){
        this.user[0].recipesUpload = []
      }
    }
  }

  configureImage(image) {
    this.photos = this.photos.filter(photo => photo != image);
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }

    this.recipe.date = new Date().toISOString().slice(0, 10)
    this.camera.getPicture(options).then((imageData) => {
      var photo = 'data:image/jpeg;base64,' + imageData
      this.photos.push(photo);
      const pictures = storage().ref('pictures');
      pictures.putString(photo, 'data_url');
    }, (err) => {
      
    })
  }

  async addRecipe(){
    for(var i = 0;i< this.photos.length;i++){
      var storageRef = storage().ref();
      var file = this.photos[i];
      this.referencePhoto = 'images/pictures'+Date.now()+'.jpg';
      var mountainImagesRef = storageRef.child(this.referencePhoto);
      const uploadImage = (file) => {
        return new Promise((resolve, reject) => {
          mountainImagesRef.putString(file, 'data_url').then((snapshot)=> {
             resolve();
          });
        });
      }

      await uploadImage(file);

      const downloadImage = () => {
        return new Promise((resolve, reject) => {
          storageRef.child(this.referencePhoto).getDownloadURL().then((url)=>{
            this.recipe.images.push(url);
            resolve();
          });
        });
      }

      await downloadImage();

      if(this.recipe.images.length == this.photos.length){
        this.recipe.id = Date.now();
        this.recipe.creator = this.user[0].username;
        this.recipe.imageCreator = this.user[0].image;

        this.usersService.getUser(this.user[0].id).valueChanges().subscribe(data => this.user[0] = data);
        if(this.user[0].recipesUpload === undefined){
          this.user[0].recipesUpload = [];
          this.user[0].recipesUpload.push(this.recipe);
        } else {
          this.user[0].recipesUpload.push(this.recipe);
        }
        

        this.recipesService.createRecipe(this.recipe);
        this.usersService.editUser(this.user[0]);

        this.photos = [];
        this.recipe.images = [];
        this.recipe.title = null;
        this.recipe.description = null;
        this.recipe.ingredients = null;
        this.recipe.productions = null;
      }
    }
    this.presentToast();   
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Receta creada correctamente',
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
