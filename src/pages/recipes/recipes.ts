import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecipesService } from '../../services/recipes.service';
import { RecipeDetailPage } from '../recipe-detail/recipe-detail';
import { UsersService } from '../../services/users.service';
import { informationAboutUser } from "../profile/profile";
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage {
  user: Array<any> = [];
  recipes = [];
  recipesCopy = [];
  searchItem: string = '';
  recipeMark: string =  "../../assets/imgs/unmark.png";
  likes: number = 0;
  changeState: boolean = false;
  heart: string = "../../assets/imgs/instagram-heart.png";
  
  @ViewChild("myNav") nav: NavController

  constructor(public navCtrl: NavController, public recipesService: RecipesService, public usersService: UsersService, private toastCtrl: ToastController) {
    recipesService.getRecipes().valueChanges().subscribe(data => this.recipes = data);
    this.user = informationAboutUser;
    if(this.user.length > 0){
      if(this.user[0].favourites === undefined){
        this.user[0].favourites = []
      }
    }
   
  }

  initializeRecipes(): void {
    this.recipesService.getRecipes().valueChanges().subscribe(data => this.recipes = data);
  }

  onSelect(recipe): void {
    this.navCtrl.push(RecipeDetailPage, {recipe: recipe});
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Añadido a favoritos',
      duration: 2000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  searchRecipes(){
    
    if(!this.searchItem){
      this.initializeRecipes();
    }
    
    this.recipes = this.recipes.filter((item) => {
      return item.title.toLowerCase().indexOf(this.searchItem.toLowerCase()) > -1;
    }); 
  }

  preferredRecipes(recipe){
    if(this.user[0].favourites === undefined){
      this.user[0].favourites = []
    }
    this.user[0].favourites.push(recipe);
    this.usersService.editUser(this.user[0]);
    this.presentToast();
  }

}
