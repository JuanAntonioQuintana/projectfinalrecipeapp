import { RecipeDetailPage } from './../recipe-detail/recipe-detail';
import { EditProfilePage } from './../edit-profile/edit-profile';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { storage } from 'firebase';
import { UsersService } from '../../services/users.service';
import { RecipesService } from '../../services/recipes.service';
import { AboutPage } from '../about/about';

export const informationAboutUser =[]

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  recipes = [];
  recipesTest = [];
  user: Array<any> = [];
  photo: string = '';
  referencePhoto: string

  @ViewChild("myNav") nav: NavController

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera:Camera, public usersService: UsersService, public recipesService: RecipesService) {
    this.user = this.navParams.get("user");
    informationAboutUser.push(this.user[0]);
    this.recipesService.getRecipes().valueChanges().subscribe(data => this.recipes = data);
    this.usersService.getUser(this.user[0].id).valueChanges().subscribe(data => this.user[0] = data);
  }

  ionViewDidEnter() {
    console.log("hello world");
  }

  onSelectUser(user): void {
    this.navCtrl.push(EditProfilePage, {user: this.user});
  }

  onSelect(recipe): void {
    this.navCtrl.push(RecipeDetailPage, {recipe: recipe});
  }

  logout(){
    informationAboutUser.pop();
    this.navCtrl.push(AboutPage);
  }

  configureImage() {
    var storageRef = storage().ref();
    this.referencePhoto = 'images/pictures'+Date.now()+'.jpg';
    var mountainImagesRef = storageRef.child(this.referencePhoto);
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }
    this.camera.getPicture(options).then(async (imageData) => {
      this.photo = 'data:image/jpeg;base64,' + imageData;
      var file = this.photo;
      await uploadImage(file);
      await downloadImage();
      this.usersService.editUser(this.user[0]);
    }, (err) => {
      
    })

    const uploadImage = (file) => {
      return new Promise((resolve, reject) => {
        mountainImagesRef.putString(file, 'data_url').then((snapshot)=> {
           resolve();
        });
      });
    }

    const downloadImage = () => {
      return new Promise((resolve, reject) => {
        storageRef.child(this.referencePhoto).getDownloadURL().then((url)=>{
          this.user[0].image = url;
          resolve();
        });
      });
    }
  }

  remove(recipe){
    this.recipesService.deleteRecipe(recipe);
    this.user[0].recipesUpload = this.user[0].recipesUpload.filter(item => item.id !== recipe.id);
    this.usersService.editUser(this.user[0]);
  }

}
