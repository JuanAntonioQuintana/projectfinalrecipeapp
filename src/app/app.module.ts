import { ProfilePage } from './../pages/profile/profile';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from './../pages/tabs/tabs';
import { RecipesPage } from './../pages/recipes/recipes'
import { FavoritesPage } from './../pages/favorites/favorites'
import { AboutPage } from './../pages/about/about';
import { NewRecipePage } from './../pages/new-recipe/new-recipe'
import { Camera } from '@ionic-native/camera';
import { RecipesService } from '../services/recipes.service';
import { UsersService } from '../services/users.service';
import { RecipeDetailPage } from '../pages/recipe-detail/recipe-detail';
import { RegisterPage } from '../pages/register/register';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyCxN3zfWCEY8ffZodidCsBf8ThJi9vs-84",
  authDomain: "finalproject-8c117.firebaseapp.com",
  databaseURL: "https://finalproject-8c117.firebaseio.com",
  projectId: "finalproject-8c117",
  storageBucket: "finalproject-8c117.appspot.com",
  messagingSenderId: "337026167961"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    RecipesPage,
    NewRecipePage,
    FavoritesPage,
    AboutPage,
    RecipeDetailPage,
    RegisterPage,
    ProfilePage,
    EditProfilePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    RecipesPage,
    NewRecipePage,
    FavoritesPage,
    AboutPage,
    RecipeDetailPage,
    RegisterPage,
    ProfilePage,
    EditProfilePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RecipesService,
    UsersService
  ]
})
export class AppModule {}
