import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class RecipesService {
    constructor(public afDB: AngularFireDatabase){}

    recipes = [];

    public getRecipes(){
        //return this.recipes;
        return this.afDB.list('recipes/');
    }

    public createRecipe(recipe){
        this.afDB.database.ref('recipes/'+recipe.id).set(recipe);
    }

    public getRecipe(id){
        //return this.recipes.filter((recipe, index)=> {return recipe.id === id})[0] || {id:null, title:null, description:null};
        return this.afDB.object('recipes/'+id);
    }

    public editRecipe(recipe){
        this.afDB.database.ref('recipes/'+recipe.id).set(recipe);
    }

    public deleteRecipe(recipe){
        this.afDB.database.ref('recipes/'+recipe.id).remove();
    }
}