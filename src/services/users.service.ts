import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class UsersService {
    constructor(public afDB: AngularFireDatabase){}

    users = [];

    public getUsers(){
        //return this.recipes;
        return this.afDB.list('users/');
    }

    public createUser(user){
        this.afDB.database.ref('users/'+user.id).set(user);
    }

    public getUser(id){
        //return this.recipes.filter((recipe, index)=> {return recipe.id === id})[0] || {id:null, title:null, description:null};
        return this.afDB.object('users/'+id);
    }

    public editUser(user){
        this.afDB.database.ref('users/'+user.id).set(user);
    }

    public deleteUser(user){
        this.afDB.database.ref('users/'+user.id).remove();
    }
}